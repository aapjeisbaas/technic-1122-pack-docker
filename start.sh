#!/bin/bash

file="/opt/technic/server.properties"

if [ ! -f "$file" ]
then
    cp -r /opt/technic-base/. /opt/technic
fi

java -server -Xmx8G -Dfml.queryResult=confirm -jar forge-1.12.2-14.23.5.2860.jar nogui