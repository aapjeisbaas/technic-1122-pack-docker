FROM openjdk:8-jdk
EXPOSE 25565
VOLUME [ "/opt/technic" ]
WORKDIR /opt/technic/
COPY . /tmp
RUN curl -o /tmp/technic-1122-pack.zip https://solder.endermedia.com/repository/downloads/the-1122-pack/the-1122-pack_1.5.2.zip \ 
    && unzip /tmp/technic-1122-pack.zip -d /opt/technic-base/ \
    && chmod +x /opt/technic-base/LaunchServer.sh \
    && rm /tmp/technic-1122-pack.zip \
    && echo -e "#$(date)\neula=true" > /opt/technic-base/eula.txt \
    && sed -i 's/^motd.*/motd=Aapje is Baas - 1.12.2 Pack v1.5.2/g' /opt/technic-base/server.properties \
    && cp /tmp/logo-aapjeisbaas.png /opt/technic-base/server-icon.png \
    && groupadd -g 1000 java \
    && useradd -m -u 1000 -g java java \
    && chown -R java:java /opt/technic-base \
    && cp /tmp/start.sh /opt/start.sh
USER java

CMD ["/opt/start.sh"]
